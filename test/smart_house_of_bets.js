const CoinHouseOfBets = artifacts.require('CoinHouseOfBets')
const SmartHouseOfBets = artifacts.require("SmartHouseOfBets");
const SmartBetMatch = artifacts.require("SmartBetMatch");

function hobToWei(){
  return 1024 * Math.pow(10, 18);
}

contract('SmartHouseOfBets', function(accounts) {

  it("Deploy SmartHouseOfBets and SmartBetMatch, test AddSmartBetMatch", function() {
    var smartHouseOfBets;

    return SmartHouseOfBets.deployed().then(function(instance) {
      smartHouseOfBets = instance;
      return SmartBetMatch.deployed().then(function(instanceMatch) {
        return smartHouseOfBets.AddSmartBetMatch(instanceMatch.address);
      }).then(function(result){  
        assert.notEqual(result.logs[0].args._contract, 0x0000000000000000000000000000000000000000, 'SmartBetMatch Return create is null?');
      })  
    })    
  })  

  it("SmartBetMatch test IntanceEventBet", function() {
    var smartBetMatch;

    return SmartBetMatch.deployed().then(function(instance) {
      smartBetMatch = instance;

      var urlApi = 'json(https://gitlab.com/ncatalogna/SmartWorldCup/raw/master/data/data.sol.json).knockout.round_2.matches[0]';
      var name = 64;
      var home = 61;
      var away = 62;
      const timeMatch = '1531634400';

      return smartBetMatch.IntanceEventBet(urlApi, name, home, away, timeMatch);
    }).then(function(result){ 
      assert.equal(result.logs[0].args.description, "Oraclize query was sent finish, standing by for the answer..", 'IntanceEventBet Return event correct');
    })       
  }) 

  it("Buy Tokens Player 1 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 200000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[1], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[1]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[1] should have 1024 HOB");       
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[1] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[1], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(2, betCoinSend, { from: accounts[1] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  })  
  it("Buy Tokens Player 2 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 100000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[2], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[2]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[2] should have 1024 HOB");  
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[2] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[2], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(2, betCoinSend, { from: accounts[2] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  }) 

  it("Buy Tokens Player 3 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 50000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[3], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[3]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[3] should have 1024 HOB");  
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[3] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[3], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(2, betCoinSend, { from: accounts[3] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  }) 

  it("Buy Tokens Player 4 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 250000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[4], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[4]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[4] should have 1024 HOB");  
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[4] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[4], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(1, betCoinSend, { from: accounts[4] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  }) 

  it("Buy Tokens Player 5 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 90000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[5], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[5]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[5] should have 1024 HOB");  
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[5] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[5], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(1, betCoinSend, { from: accounts[5] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  }) 

  it("Buy Tokens Player 6 and in game", function() {
    var coinHouseOfBets;
    var smartBetMatch;
    var betCoinSend = 40000000000000000000;
    return  CoinHouseOfBets.deployed().then(function(instance) {
      coinHouseOfBets = instance;
      return web3.eth.sendTransaction({to: coinHouseOfBets.address, from: accounts[6], value: web3.toWei('1', 'ether')})
    }).then(function(result){
        return coinHouseOfBets.balanceOf.call(accounts[6]);
    }).then(function(result){ 
      assert.equal(result.toNumber(), hobToWei(), "accounts[6] should have 1024 HOB");  
      return SmartBetMatch.deployed().then(function(instance) {        
        smartBetMatch = instance;  
        return coinHouseOfBets.approve(smartBetMatch.address, betCoinSend, { from: accounts[6] });
    }).then(function(result){  
        assert.equal(result.logs[0].args.tokenOwner, accounts[6], 'approve tokenOwner event correct');
        assert.equal(result.logs[0].args.spender, smartBetMatch.address, 'approve spender event correct');
        assert.equal(result.logs[0].args.tokens, betCoinSend, 'approve Return tokens correct');
        return smartBetMatch.InPlayerBet(1, betCoinSend, { from: accounts[6] });
      }).then(function(result){ 
          assert.equal(result.logs[0].args.description, "Player add in Game. ", 'InPlayerBet Return event correct');
      })
    })           
  }) 
    
  it("SmartBetMatch test Update post IntanceEventBet", function(done) {
    var smartBetMatch;
    this.timeout(20000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.Update({ from: accounts[0], value: web3.toWei('1', 'ether') });
      }).then(function(result){         
        assert.equal(result.logs[0].args.description, 'Oraclize query was sent winner, standing by for the answer..', 'IntanceEventBet Return event correct');
        done();
      });  
    }, 15000);      
  }) 

  it("SmartBetMatch test ReclaimPlayerBet Player 1 and loser", function(done) {
    var smartBetMatch;
    this.timeout(25000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.ReclaimPlayerBet({ from: accounts[1] });
      }).then(function(result){ 
        assert.equal(result.logs[0].args.description, 'You selected a home away and winner is home. ', 'IntanceEventBet Return event correct');
        done();
      });  
    }, 10000);      
  }) 

  it("SmartBetMatch test ReclaimPlayerBet Player 4 and winner", function(done) {
    var smartBetMatch;
    this.timeout(26000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.ReclaimPlayerBet({ from: accounts[4] });
      }).then(function(result){ 
        assert.equal(result.logs[1].args.description, 'Your game has been finalized and your money has been sent. ', 'IntanceEventBet Return event correct');
        assert.equal(result.logs[1].args.tokenWineer.valueOf(), 477500000000000000000, 'IntanceEventBet tokenWineer Return event correct');
        return  CoinHouseOfBets.deployed().then(function(instance) {
          coinHouseOfBets = instance;
          return coinHouseOfBets.balanceOf.call(accounts[4]);          
        }).then(function(result){ 
          assert.equal(result.valueOf(), 1.2515e+21, "accounts[4] should have 1251.5 HOB");  
          done();
        })

      });  
    }, 10000);      
  }) 

  it("SmartBetMatch test ReclaimPlayerBet 4 player claims for the second time ", function(done) {
    var smartBetMatch;
    this.timeout(26000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.ReclaimPlayerBet.call({ from: accounts[4] })
        .then((ex) => {    
          assert.ok(false, "It didn't fail");
          done();
        }, (e) => {
          if(e.toString().indexOf("revert") != -1) { // Refactory for validate message revert
            assert.ok(true, "Passed");             
          } else {
            assert(false, error.toString());            
          }
          done();
        });        
      }) 
    }, 10000);      
  }) 
  
  it("SmartBetMatch test ReclaimPlayerBet Player 5 and winner", function(done) {
    var smartBetMatch;
    this.timeout(27000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.ReclaimPlayerBet({ from: accounts[5] });
      }).then(function(result){ 
        assert.equal(result.logs[1].args.description, 'Your game has been finalized and your money has been sent. ', 'IntanceEventBet Return event correct');
        assert.equal(result.logs[1].args.tokenWineer.valueOf(), 170500000000000000000, 'IntanceEventBet tokenWineer Return event correct');
        return  CoinHouseOfBets.deployed().then(function(instance) {
          coinHouseOfBets = instance;
          return coinHouseOfBets.balanceOf.call(accounts[5]);          
        }).then(function(result){ 
          assert.equal(result.valueOf(), 1.1045e+21, "accounts[5] should have 1104.5 HOB");  
          done();
        })

      }); 
    }, 10000);      
  }) 

  it("SmartBetMatch test ReclaimPlayerBet Player 6 and winner", function(done) {
    var smartBetMatch;
    this.timeout(28000);
    setTimeout(function () {
      return SmartBetMatch.deployed().then(function(instance) {
        smartBetMatch = instance;
        return smartBetMatch.ReclaimPlayerBet({ from: accounts[6] });
      }).then(function(result){ 
        assert.equal(result.logs[1].args.description, 'Your game has been finalized and your money has been sent. ', 'IntanceEventBet Return event correct');
        assert.equal(result.logs[1].args.tokenWineer.valueOf(), 75000000000000000000, 'IntanceEventBet tokenWineer Return event correct');
        return  CoinHouseOfBets.deployed().then(function(instance) {
          coinHouseOfBets = instance;
          return coinHouseOfBets.balanceOf(accounts[6]);          
        }).then(function(result){ 
          assert.equal(result.valueOf(), 1.059e+21, "accounts[6] should have 1059 HOB");  
          done();
        })

      }); 
    }, 10000);      
  }) 
});
