#Lógica de reparto:

El equipo ganador tendrá un porcentaje total del perdedor, menos la comisión por los costos del servicio.
Cada jugador se le calculará su porcentaje de beneficios a base de su participación.

**El calculo el Coeficiente:**

* **'c'** Coeficiente 
* **'y'** Equipo Equipo A
* **'x'** Equipo Equipo B

$`\Gamma(c) =  (y + x) / x)`$

$`\Gamma(c') =  (y + x) / y)`$


**Ejemplo:**

    Los siguientes valores son en Token vendidos para ingresar a la apuesta.

* *Equipo A:*
	* Jugador 1: 100
	* Jugador 2: 250
	* Jugador 3: 50
* *Equipo B:*
	* Jugador 4: 200
	* Jugador 5: 60
	* Jugador 6: 90

*Totales:*
* Equipo A: 400 Token
* Equipo B: 350 Token

**El calculo de Ganancia:**

* **'p'** Total del equipo Participante
* **'x'** Participacion en Tokes
* **'d'** Total de equipo desfavorecido
* **'q'** % Participacion
* **'g'** Ganado
* **'z'** Total Ganado
* **'c'** Coeficiente

* % Participacion: $`( x / p ) * 100 = q`$
* Ganado: $`(d / 100) * q = g`$
* Total Ganado: $`( g + x ) = z`$
* Coeficiente: $`( z / p ) = c`$

El equipo Ganador es el **A**

* *Jugador 1*: 100
    * % Participacion: (100/400)*100 = 25 
    * Ganado: (350/100)*25 = 87,5
    * Total Ganado: (87,5 + 100) = 187,5
    * Coeficiente: (187,5/100) = 1,875
* *Jugador 2*: 250
    * % Participacion: (250/400)*100 = 62,5
    * Ganado: (350/100)*62.5 = 218,75
    * Total Ganado: (218,75 + 250) = 468,75
    * Coeficiente: (468,75/250) = 1,875
* *Jugador 3*: 50
    * % Participacion: (50/400)*100 = 12,5
    * Ganado: (350/100)*12,5 = 43,75
    * Total Ganado: (43,75 + 50) = 93,75
    * Coeficiente: (93,75/50) = 1,875

El equipo Ganador: Equipo B

* *Jugador 4*: 200
    * % Participacion: (200/350)*100 = 57,14
    * Ganado: (400/100)*57,14 = 228,56
    * Total Ganado: (228,56 + 200) = 428,56
    * Coeficiente: (428,56/200) = 2,1428
* *Jugador 5*: 60
    * % Participacion: (60/350)*100 = 17,14
    * Ganado: (400/100)*17,14 = 68,56
    * Total Ganado: (68,56 + 60) = 128,56
    * Coeficiente: (128,56/60) = 2,1426
* *Jugador 6*: 90
    * % Participacion: (90/350)*100 = 25,71
    * Ganado: (400/100)*25,71 = 102,84
    * Total Ganado: (102,84 + 90) = 192,84
    * Coeficiente: (192,84/90) = 2,1426


