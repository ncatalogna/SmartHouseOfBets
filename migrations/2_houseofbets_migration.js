var CoinHouseOfBets = artifacts.require("CoinHouseOfBets");
var SmartHouseOfBets = artifacts.require("SmartHouseOfBets");
var SmartBetMatch = artifacts.require("SmartBetMatch");

module.exports = function(deployer) {
  // deployment steps
  deployer.deploy(CoinHouseOfBets).then(function() {    
    return deployer.deploy(SmartHouseOfBets, CoinHouseOfBets.address).then(function() {                  
      // In deploy prod, eliminate address ora 0x6f485c8bf6fc43ea212e93bbf8ce046c7f1cb475 localhost
      return deployer.deploy(SmartBetMatch, CoinHouseOfBets.address, 0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
    });
  });
};

